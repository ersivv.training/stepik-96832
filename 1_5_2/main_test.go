package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_digits(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "#1",
			args: args{
				s: "sdfsdfkm1sdf",
			},
			want: true,
		},
		{
			name: "#2",
			args: args{
				s: "sdfsdfkmsdf",
			},
			want: false,
		},
		{
			name: "#3",
			args: args{
				s: "####!@!##$@#$",
			},
			want: false,
		},
		{
			name: "#3",
			args: args{
				s: "12121",
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := digits(tt.args.s); got != tt.want {
				t.Errorf("digits() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_letters(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "#1",
			args: args{
				s: "sdfsdfkm1sdf",
			},
			want: true,
		},
		{
			name: "#2",
			args: args{
				s: "sdfsdfkmsdf",
			},
			want: true,
		},
		{
			name: "#3",
			args: args{
				s: "####!@!##$@#$",
			},
			want: false,
		},
		{
			name: "#3",
			args: args{
				s: "12121",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := letters(tt.args.s); got != tt.want {
				t.Errorf("letters() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_minlen(t *testing.T) {
	type args struct {
		s      string
		length int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "#1",
			args: args{
				s:      "1",
				length: 10,
			},
			want: false,
		},
		{
			name: "#1",
			args: args{
				s:      "0123456789",
				length: 10,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, minlen(tt.args.length)(tt.args.s), tt.want)
		})
	}
}

func Test_password_isValid(t *testing.T) {
	type fields struct {
		value     string
		validator validator
	}
	tests := []struct {
		name   string
		fields fields
		want   bool
	}{
		{
			name: "#1",
			fields: fields{
				value:     "hellowor1d",
				validator: or(and(digits, letters), or(minlen(10))),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &password{
				value:     tt.fields.value,
				validator: tt.fields.validator,
			}
			if got := p.isValid(); got != tt.want {
				t.Errorf("isValid() = %v, want %v", got, tt.want)
			}
		})
	}
}
