package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_tournament_calcRating(t *testing.T) {
	type args struct {
		trnStr string
	}
	tests := []struct {
		name string
		args args
		want rating
	}{
		{
			name: "test",
			args: args{
				trnStr: "ABW DCD DAW CBL BDL ACW",
			},
			want: rating{
				'A': 6,
				'B': 3,
				'C': 1,
				'D': 7,
			},
		},
		{
			name: "#3",
			args: args{
				trnStr: "ABW DAD CBL ACW BDD CDL",
			},
			want: rating{
				'A': 3 + 1 + 0 + 3 + 0 + 0,
				'B': 0 + 0 + 3 + 0 + 1 + 0,
				'C': 0 + 0 + 0 + 0 + 0 + 0,
				'D': 0 + 1 + 0 + 3 + 1 + 0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			trn := parseTournament(tt.args.trnStr)
			assert.NotNil(t, trn)
			rt := trn.calcRating()
			assert.Equal(t, rt, tt.want)
		})
	}
}
